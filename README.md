# Accumulate Improvement Proposals (AIP) :gear:

This is the central location for collaboration on technical aspects and improvements of Accumulate. 

The intention is to formalise specifications that implementors should adhere to when building on Accumulate in order to encourage ecosystem improvement and interoperability.

# How Does AIP Work?

See [AIP-X](AIP/x.md), the initial standard that defines the AIP process.

# AIP List

## Work In Progress

| AIP | Name | Category |
| --- | ---- | -------- |
|     |      |          |

## Draft

| AIP           | Name                                                                                                  | Category |
| ------------- | ----------------------------------------------------------------------------------------------------- | -------- |
| [X](AIP/x.md) | Standard, Contributions, and Process Guidelines for Accumulate | Meta     |
|               |                                                                                                       |          |

## Last Call

| AIP | Name | Category |
| --- | ---- | -------- |
|     |      |          |

## Accepted

| AIP | Name | Category |
| --- | ---- | -------- |
|     |      |          |

## Final

| AIP | Name | Category |
| --- | ---- | -------- |
|     |      |          |

# Contributing

[AIP-X](AIP/x.md) is the initial standard that describes the core framework,
conventions and guidelines for creating an AIP submission. Create your submission in a 
markdown file following the guidelines defined in AIP-X and the [AIP Template](AIP/template.md)

To submit your specification, fork this repo and then submit a pull request to have
it reviewed and included for evaluation by the core committee.

# Editors

Our current list of editors and curators:

-   **Niels Klomp** - <nklomp@sphereon.com>

# Copyright

Copyright and related rights waived via
[CC0](https://creativecommons.org/publicdomain/zero/1.0/).
